import re
import io
import datetime
import time
import itertools
import pickle

#'<div>http://www.espn.com/nba/scoreboard/_/date/19920101</div>[]<div>'
def separo(f, linesep):
	dts = ""
	while True:
		while linesep in dts:
			pos = dts.index(linesep)
			yield dts[:pos]
			dts = dts[pos + len(linesep):]
		cec = f.read()
		if not cec:
			yield dts
			break
		dts += cec
def is_odd(a):
	return bool(a - ((a>>1)<<1))
trimd6F = open( 'done.html', 'w' )
trimd6csv = open( 'done.csv', 'w' )
with open('espnNBA1.html', 'r') as f:
	awayStr={}
	homeStr={}
	awayScore={}
	rowIndex=0
	tRurowIndex=0
	matchDatesDict={}
	homeTeamDict={}
	awayTeamDict={}
	winnerDict={}
	skorecelkem={}
	skorecelkemI=0

	helper1={}
	helper2={}
	helper3={}
	helper4={}

	totalka={}
	helper3.clear()
	awayB=True
	homeB=False
	longer=""
	longerDict={}
	vseckyDataStr=""
	vseckyData={}
	start_time = time.time()
	trimd6F.write("<table><tbody>")
	for line in separo(f, "@"):
		if re.match('<\/div><div>http:\/\/www.espn.com\/nba\/scoreboard\/_\/date\/(\d+)<\/div>\[<section', line):
			totalka = ''.join(map(str, line))
			for datum in re.finditer('<\/div><div>http:\/\/www.espn.com\/nba\/scoreboard\/_\/date\/(\d+)<\/div>\[<section', line, re.S):	
				helper4 = ''.join(map(str, datum.group(1)))
				helper4 = datetime.datetime.strptime(helper4, "%Y%m%d")
				helper4 = helper4.strftime('%Y-%d-%m')
				longer=longer+helper4+"@"
				rowIndex=rowIndex+1
				matchDatesDict['item'+str(rowIndex)+''] = helper4
			cDb=0
			for homawhomeSc in re.finditer('<tr class="away">(?P<away>.*?)</tr>|<tr class="home">(?P<home>.*?)</tr>', line, re.S):	
				cDb=cDb+1
				if is_odd(cDb):
					awayB=True
					homeB=False
				else:
					awayB=False
					homeB=True
				
				if awayB is True:
					awayStr = ''.join(map(str, homawhomeSc.group(1)))
					trimd6F.write("<tr>")
				if homeB is True:
					homeStr = ''.join(map(str, homawhomeSc.group(2)))
					
				trimd6F.write("<td class='datum'>"+helper4+"</td>")
				trimd6csv.write(helper4+",")
				vseckyDataStr=vseckyDataStr+"@"+helper4
				if homeStr:
					for homeTnm in re.finditer('<span class="sb-team-short">(.*?)</span>', homeStr):
						homeTeam = homeTnm.group(1)
						trimd6F.write("<td class='domaci'>"+homeTeam+"</td>")
						homeTeamDict['item'+str(rowIndex)+''] = homeTeam
						trimd6csv.write(homeTeam+",")
					b=0
					for homeSc in re.finditer('<td class="score">(.*?)</td>', homeStr, re.S):	
		
						b=b+1
						trimd6F.write("<td class='domaciSkoreQ"+str(b)+"'>"+homeSc.group(1)+"</td>")
						trimd6csv.write(homeSc.group(1)+",")
					if b==4:
						trimd6F.write("<td class=\"domaciSkoreQ5\">-</td>")
						trimd6csv.write("-,")
					b=0
					for homTt in re.finditer('<td class="total"><span>(.*?)</span>', homeStr):
						homeScoreT = homTt.group(1)
						trimd6F.write("<td class='domaciSkoreT"+str(b)+"'>"+homeScoreT+"</td>")
						trimd6csv.write(homeScoreT+",")
						vseckyDataStr=vseckyDataStr+"@"+homeScoreT
						#XXXXXXX
				homeStr=None
				
				if awayStr:
					for awayTnm in re.finditer('<span class="sb-team-short">(.*?)</span>', awayStr):
						awayTeam = awayTnm.group(1)
						trimd6F.write("<td class='hoste'>"+awayTeam+"</td>")
						trimd6csv.write(awayTeam+",")
						awayTeamDict['item'+str(rowIndex)+''] = awayTeam
					b=0
					for awaSc in re.finditer('<td class="score">(.*?)</td>', awayStr, re.S):	
		
						b=b+1
						trimd6F.write("<td class='hosteSkoreQ"+str(b)+"'>"+awaSc.group(1)+"</td>")
						trimd6csv.write(awaSc.group(1)+",")
					if b==4:
						trimd6F.write("<td class=\"hosteSkoreQ5\">-</td>")
						trimd6csv.write("-,")
					b=0
					for awaTt in re.finditer('<td class="total"><span>(.*?)</span>', awayStr):
						awaScoreT = awaTt.group(1)
						trimd6F.write("<td class='hosteSkoreT"+str(b)+"'>"+awaScoreT+"</td>")
						trimd6csv.write(awaScoreT+",")
						vseckyDataStr=vseckyDataStr+"@"+awaScoreT

				awayStr=None

				if awayB is True:
					
					awayB=False
				if homeB is True:
					if int(awaScoreT) > int(homeScoreT):
						winnerDict['item'+str(rowIndex)+''] = awayTeam
						winnerStr = awayTeam
					else:
						winnerDict['item'+str(rowIndex)+''] = homeTeam
						winnerStr = homeTeam
					skorecelkemI=int(awaScoreT)+int(homeScoreT)
					skorecelkem['item'+str(rowIndex)+''] = skorecelkemI
					trimd6F.write("<td class='winner'>"+winnerStr+"</td>")
					trimd6F.write("<td class='totalScore'>"+str(skorecelkemI)+"</td>")
					trimd6F.write("</tr>")
					trimd6csv.write(winnerStr+",")
					trimd6csv.write(str(skorecelkemI))
					longer=longer+winnerStr
					longerDict['item'+str(rowIndex)+''] = longer
					trimd6csv.write("\n")
					vseckyDataStr=vseckyDataStr+"@"+winnerStr+"@"+str(skorecelkemI)+"\n"
					tRurowIndex=tRurowIndex+1
					vseckyData['item'+str(tRurowIndex)+''] = vseckyDataStr
					longer=""
					vseckyDataStr=""
					homeB=False
				
				
		
	trimd6F.write("</tbody></table>")
trimd6F.close()
trimd6csv.close()

with open('results.pkl', 'wb') as handle:
	pickle.dump(longerDict, handle, protocol=pickle.HIGHEST_PROTOCOL)
with open('vsecko.pkl', 'wb') as handle:
	pickle.dump(vseckyData, handle, protocol=pickle.HIGHEST_PROTOCOL)
with open('matchDatesDict.pkl', 'wb') as handle:
	pickle.dump(matchDatesDict, handle, protocol=pickle.HIGHEST_PROTOCOL)

with open('homeTeamDict.pkl', 'wb') as handle:
	pickle.dump(homeTeamDict, handle, protocol=pickle.HIGHEST_PROTOCOL)

with open('awayTeamDict.pkl', 'wb') as handle:
	pickle.dump(awayTeamDict, handle, protocol=pickle.HIGHEST_PROTOCOL)

with open('winnerDict.pkl', 'wb') as handle:
	pickle.dump(winnerDict, handle, protocol=pickle.HIGHEST_PROTOCOL)


print("--- %s seconds ---" % (time.time() - start_time))
