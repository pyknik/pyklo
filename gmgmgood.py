from pdfminer.pdfparser import PDFParser, PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import LAParams, LTTextBox, LTTextLine, LTFigure, LTImage


import sys
#import hashlib
#import multiprocessing
import os
import time
#import threading
#import wmi


start_time = time.time()
slozka='C:\\aukce\\6test\\'''
images_folder='C:\\aukce\\neco\\'''
#c = wmi.WMI()
d=0


for f in os.listdir(slozka):
	fnm=str(f)
	with open(slozka+fnm, 'rb') as r:
		if not fnm.startswith('4'):
			while (d<9):
				d=d+1
				parser = PDFParser(r)
				doc = PDFDocument()
				parser.set_document(doc)
				doc.set_parser(parser)
				doc.initialize('')
				rsrcmgr = PDFResourceManager()
				laparams = LAParams()
				device = PDFPageAggregator(rsrcmgr, laparams=laparams)
				interpreter = PDFPageInterpreter(rsrcmgr, device)
				print('@#@#@##@'+fnm+'@#@#@##@')
				text_content = []
				for page in doc.get_pages():
					interpreter.process_page(page)
					layout = device.get_result()
					for lt_obj in layout:
						if isinstance(lt_obj, LTTextBox) or isinstance(lt_obj, LTTextLine):
							text_content.append(lt_obj.get_text())
							print(lt_obj.get_text())
						elif isinstance(lt_obj, LTImage):
							# an image, so save it to the designated folder, and note it's place in the text 
							saved_file = save_image(lt_obj, page_number, images_folder)
							if saved_file:
								# use html style <img /> tag to mark the position of the image within the text
								text_content.append('<img src="'+os.path.join(images_folder, saved_file)+'" />')
							else:
								print >> sys.stderr, "Error saving image on page", page_number, lt_obj.__repr__
						#elif isinstance(lt_obj, LTFigure):
						#	# LTFigure objects are containers for other LT* objects, so recurse through the children
						#	text_content.append(lt_obj.objs, page_number, images_folder, text_content)
					print(text_content)
#print("--- %s seconds ---" % (time.time() - start_time))